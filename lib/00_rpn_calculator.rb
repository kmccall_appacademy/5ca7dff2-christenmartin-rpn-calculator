class RPNCalculator
  attr_reader :value

  def initialize
    @stack = []
    @operations = [:+, :/, :-, :*]
  end

  def value
    @stack.last
  end

  def push(num)
    @stack << num
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def perform_operation(op)
    if @stack.length < 2
      raise "calculator is empty"
    end

    num_two = @stack.pop
    num_one = @stack.pop

    if op == :+
      @stack << num_one + num_two
    elsif op == :-
      @stack << num_one - num_two
    elsif op == :*
      @stack << num_one * num_two
    elsif op == :/
      @stack << num_one.to_f / num_two.to_f
    end
  end

  def tokens(string)
    string.split.map do |token|
      if @operations.include?(token.to_sym)
        token.to_sym
      else
        token.to_i
      end
    end
  end


  def evaluate(string)
    string.split.each do |token|
      if @operations.include?(token.to_sym)
        perform_operation(token.to_sym)
      else
        @stack << token.to_i
      end
    end

    value
  end

end
